import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../models/dark_theme_provider.dart';
import '../utilities/light_roll_switch.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    final themeChange = Provider.of<DarkThemeProvider>(context);

    return Scaffold(
        body: const Placeholder(),
        appBar: AppBar(
          title: Container(
            padding: const EdgeInsets.symmetric(vertical: 10),
            margin: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                LightDarkSwitch(
                  value: themeChange.darkTheme,
                  textOn: 'Dark',
                  textOnColor: const Color(0xff8c8c8c),
                  textOff: 'Light',
                  textOffColor: const Color(0xff8c8c8c),
                  colorOn: const Color(0xff333332),
                  colorOff: const Color(0xffe6e6e6),
                  iconOn: Icons.light_mode,
                  iconOff: Icons.dark_mode,
                  textSize: 16.0,
                  animationDuration: const Duration(milliseconds: 200),
                  onChanged: (bool state) {
                    setState(() {
                      themeChange.darkTheme = state;
                    });
                  },
                  onTap: () {},
                  onDoubleTap: () {},
                  onSwipe: () {},
                ),
              ],
            ),
          ),
        ));
  }
}
